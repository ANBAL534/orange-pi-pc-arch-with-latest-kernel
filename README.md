How to update the kernel and use the latest version in Arch ARM in Orange Pi PC the bad but working way

  

We will need a *UART USB* (ex.: an arduino) and the programs *screen, dtc, swig, uboot-tools*

  

### Prepare the cross toolchain:

  

    $ yay -S crosstool-ng-git
    $ mkdir toolchain
    $ cd toolchain
    $ ct-ng menuconfig

  

- Add the next configurations in the shown menu:

  

*Paths and misc option \--\> Prefix directory : /opt/x-tools/\${CT\_TARGET}*

  

*Target options \--\> Target Architecture : arm*

  

*Target options \--\> Use specific FPU : vfp*

  

*Target options \--\> Floating point: hardware (FPU)*

  

*Target options \--\> Use EABI : \**

  

*Target options \--\> append \'hf\' to the tuple (EXPERIMENTAL) : \**

  

*Toolchain options \--\> Tuple\'s vendor string : opi*

  

*Operating System \--\> Target OS : linux*

  

*Operating System \--\> Linux kernel version : \[select latest\]*

  

*C compiler \--\> C compiler : gcc*

  

*C compiler \--\> gcc version : 5.3.0 or last, it doesn't matter*

  

*C compiler \--\> C++ : \**

  

`$ ct-ng build (This process might thorw some errors that are trivial to solve, permissions and whatnot)`

  

    $ export INSTALL\_MOD\_PATH=/home/anibal/Downloads/archArmH3/linux/output &&    
    export PATH=\$PATH:/opt/x-tools/arm-opi-linux-gnueabihf/bin && 
    export ARCH=arm && export CROSS\_COMPILE=arm-opi-linux-gnueabihf-

  

**Execute this last command in each new terminal you open that you plan to execute any compilation, also look out for the path, modify it as it is in your machine**

  

- Verify:

  

`$ arm-opi-linux-gnueabihf-gcc -v`

  

*\[\...\]*

  

*gcc version a.b.c date*

  

`$ echo \$ARCH\|grep arm`

  

*arm*

  

`$ echo \$CROSS\_COMPILE\|grep arm`

  

*arm-opi-linux-gnueabihf-*

  

### u-boot compilation:

  

`$ git clone git://git.denx.de/u-boot.git`

`$ cd u-boot`

  

- Here you apply the patch hardcoded-inserted-card.patch (https://gitlab.com/ANBAL534/orange-pi-pc-arch-with-latest-kernel/blob/master/hardcoded-inserted-card.patch):

  
`$ patch drivers/mmc/mmc.c hardcoded-inserted-card.patch`
  

- And commpile:

  

`$ make orangepi\_pc\_defconfig`

`$ make -j13`

  

The resulting file should be: *u-boot-sunxi-with-spl.bin*

  

### Compile Linux kernel:

  

`$ git clone https://github.com/megous/linux.git`

`$ git clone https://megous.com/git/linux-firmware`

`$ cd linux`

`$ git checkout orange-pi-5.5 \# Optional if you want a special version and not the very last`

  

`$ make orangepi_defconfig`

  

`$ make menuconfig`

  

- in the shown menu, activate:

  

*Kernel Features \--\> Enable seccomp to safely compute untrusted bytecode : \**

  

*Device Drivers \--\> Network device support \--\> Ethernet driver support \--\> STMicroelectronics devices : \**

  

*General Setup \--\> System V IPC : \**



*Device Drivers \--\> Generic Driver Options \--\> Firmware loader \--\> Firmware loading facility \**  Here you have to point to the folder you just downloaded from git holding the linux-firmware


With this you will have a minimal fully working linux distro, for extras like wifi, usbs, etc. load this config file in the menu ([https://gitlab.com/ANBAL534/orange-pi-pc-arch-with-latest-kernel/blob/master/config.config](https://gitlab.com/ANBAL534/orange-pi-pc-arch-with-latest-kernel/blob/master/config.config)) and/or add modules in the menu yourself.


- Apply the patch broken-cd-as-nonremovable.patch for versions \>=5.5 or a specific for your version ([https://gitlab.com/ANBAL534/orange-pi-pc-arch-with-latest-kernel/blob/master/broken-cd-as-nonremovable.patch](https://gitlab.com/ANBAL534/orange-pi-pc-arch-with-latest-kernel/blob/master/broken-cd-as-nonremovable.patch)):  

`$ patch drivers/mmc/core/host.c broken-cd-as-nonremovable.patch`
  

- and compile:

  

`$ make -j13 zImage dtbs modules`

  

`$ make modules_install`

  

### Prepare the SDCard, from now on /dev/sde:

  

- Erase the sdcard:

  

`$ dd if=/dev/zero of=/dev/sdX bs=1M count=8`

  

`$ sudo gparted \# Make a new partition on the whole card but the first megabyte`

  

`$ mkfs.ext4 -O \^metadata\_csum,\^64bit /dev/sde1`

  

- Mount the sdcard

  

`$ mkdir opi`

  

`$ sudo mount /dev/sde1 ./opi`

  

- Download and copy the arch's rootfs to the sdcard:

  

`$ wget http://archlinuxarm.org/os/ArchLinuxARM-armv7-latest.tar.gz`

  

`$ bsdtar -xpf ArchLinuxARM-armv7-latest.tar.gz -C ./opi/ && sync \# This will take a long time`

  

- Use this to follow the progress:

  

`$ sudo watch -n 0.2 -d grep -e Dirty: -e Writeback: /proc/meminfo`

  

- Now download boot.cmd 
https://gitlab.com/ANBAL534/orange-pi-pc-arch-with-latest-kernel/blob/master/boot.cmd
  
- Prepare the script:

`$ mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n \"Orange Pi PC boot script\" -d boot.cmd boot.scr`

  

- Copy the files to the sdcard:

  

`$ sudo cp linux/arch/arm/boot/zImage opi/boot`

  

`$ sudo cp -r linux/output/lib/modules/5.6.0-rc4-00078-g9f65ed5fe41c-dirty opi/lib/modules/`


- Copy source and link build folder in modules so you can compile things in the opi

`$ sudo rm opi/lib/modules/5.6.0-rc4-00078-g9f65ed5fe41c-dirty/build/ opi/lib/modules/5.6.0-rc4-00078-g9f65ed5fe41c-dirty/source/`
  

`$ sudo mkdir opi/lib/modules/5.6.0-rc4-00078-g9f65ed5fe41c-dirty/source/`
  

`$ sudo cp -r linux/* opi/lib/modules/5.6.0-rc4-00078-g9f65ed5fe41c-dirty/source/ && sync`
  

`$ sudo ln -s opi/lib/modules/5.6.0-rc4-00078-g9f65ed5fe41c-dirty/source opi/lib/modules/5.6.0-rc4-00078-g9f65ed5fe41c-dirty/build`
  

`$ sudo cp boot.scr opi/boot`

  

`$ sudo umount /dev/sde1`

  

- Save el u-boot in the sdcard:

  

`$ sudo dd if=u-boot-sunxi-with-spl.bin of=/dev/sde bs=1024 seek=8`

  

### You are ready, login information:

  
| USERNAME | PASSWORD |
|--|--|
| root | root |
| alarm | alarm |

  

Based greatly on the information provided by this pages:

  

- https://notsyncing.net/?p=blog&b=2016.orangepi-pc-custom-kernel

  

- https://wiki.archlinux.org/index.php/Orange\_Pi

  

- https://github.com/megous/linux/tree/orange-pi-5.3