#!/usr/bin/env bash

export INSTALL_MOD_PATH=$(pwd)/linux/output && export PATH=$PATH:/opt/x-tools/arm-opi-linux-gnueabihf/bin && export ARCH=arm && export CROSS_COMPILE=arm-opi-linux-gnueabihf-

git clone git://git.denx.de/u-boot.git
cd u-boot
patch drivers/mmc/mmc.c ../hardcoded-inserted-card.patch

make orangepi_pc_defconfig
make -j13
cd ..

git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
cd linux
cp ../config.config .config

make sunxi_defconfig
make olddefconfig

patch drivers/mmc/core/host.c ../broken-cd-as-nonremovable.patch
patch kernel/module.c ../modinfo_check.patch

make -j13 zImage dtbs modules
make modules_install
cd ..

echo "Compilation ended!"